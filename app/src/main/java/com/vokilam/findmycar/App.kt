package com.vokilam.findmycar

import android.app.Application
import android.app.NotificationManager
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import androidx.work.WorkManager
import com.vokilam.analytics.Analytics
import com.vokilam.analytics.tracker.dummy.DummyTracker
import com.vokilam.analytics.tracker.dummy.dummy
import com.vokilam.findmycar.domain.Constants
import com.vokilam.findmycar.logger.Logger
import com.vokilam.findmycar.ui.notification.NotificationChannel
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class App : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory
    @Inject
    lateinit var workManager: WorkManager
    @Inject
    lateinit var notificationManager: NotificationManager
    @Inject
    lateinit var analytics: Analytics

    override fun onCreate() {
        super.onCreate()
        Logger.init(this)
        NotificationChannel.createAllChannels(this, notificationManager)

        workManager.getWorkInfosForUniqueWorkLiveData(Constants.WORK_NAME_SAVE_LOCATION).observeForever {
            L.debug { "getWorkInfoByIdLiveData(): $it" }
        }

        analytics.trackEvent {
            name = "hello"
            putProperties("asd" to 123)
            mappings {
                dummy { addAllKeys() }
            }
        }
    }

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

    companion object {
        private val L = Logger()
    }
}