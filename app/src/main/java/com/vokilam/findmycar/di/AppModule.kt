package com.vokilam.findmycar.di

import android.content.Context
import coil.Coil
import coil.ImageLoader
import com.vokilam.findmycar.ui.notification.NotificationFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    companion object {
        @Provides
        fun provideNotificationFactory(): NotificationFactory {
            return NotificationFactory
        }

        @Provides
        fun provideImageLoader(@ApplicationContext context: Context): ImageLoader {
            // Singleton
            return Coil.imageLoader(context)
        }
    }
}