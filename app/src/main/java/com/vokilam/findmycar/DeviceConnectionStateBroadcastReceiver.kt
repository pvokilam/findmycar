package com.vokilam.findmycar

import android.app.NotificationManager
import android.bluetooth.BluetoothAdapter.*
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.vokilam.findmycar.domain.Constants.WORK_NAME_SAVE_LOCATION
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.logger.Logger
import com.vokilam.findmycar.ui.notification.NotificationFactory
import com.vokilam.findmycar.work.LocationWorker
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DeviceConnectionStateBroadcastReceiver : BroadcastReceiver() {

    @Inject lateinit var appSettings: AppSettings
    @Inject lateinit var workManager: WorkManager
    @Inject lateinit var notificationManager: NotificationManager

    override fun onReceive(context: Context, intent: Intent) {
        L.debug { "onReceive(): intent=$intent" }
        when (intent.action) {
            ACTION_CONNECTION_STATE_CHANGED -> handleConnectionStateChanged(intent)
        }
    }

    private fun handleConnectionStateChanged(intent: Intent) {
        val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE) ?: return
        val prevState = intent.getIntExtra(EXTRA_PREVIOUS_CONNECTION_STATE, -1)
        val state = intent.getIntExtra(EXTRA_CONNECTION_STATE, -1)

        if (appSettings.selectedPairedDeviceAddress != device.address) return
        L.debug { "handleConnectionStateChanged(): device=${device.name}(${device.address}), prevState=${stateToString(prevState)}, state=${stateToString(state)}" }

        when (state) {
            STATE_DISCONNECTED -> {
                L.info { "handleConnectionStateChanged(): disconnected ${device.name}(${device.address})" }
                val request = OneTimeWorkRequestBuilder<LocationWorker>().build()
                workManager.enqueueUniqueWork(WORK_NAME_SAVE_LOCATION, ExistingWorkPolicy.KEEP, request)
            }
            STATE_CONNECTED -> {
                L.info { "handleConnectionStateChanged(): connected ${device.name}(${device.address})" }
                workManager.cancelUniqueWork(WORK_NAME_SAVE_LOCATION)
                appSettings.lastLocation = null
                notificationManager.cancel(NotificationFactory.NOTIFICATION_ID_LAST_LOCATION)
            }
        }
    }

    private fun stateToString(state: Int): String {
        return when (state) {
            STATE_CONNECTED -> "CONNECTED"
            STATE_CONNECTING -> "CONNECTING"
            STATE_DISCONNECTED -> "DISCONNECTED"
            STATE_DISCONNECTING -> "DISCONNECTING"
            else -> "UNKNOWN($state)"
        }
    }

    companion object {
        private val L = Logger()
    }
}