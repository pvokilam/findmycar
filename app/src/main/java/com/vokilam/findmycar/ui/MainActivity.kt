package com.vokilam.findmycar.ui

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.vokilam.findmycar.R
import com.vokilam.findmycar.databinding.ActivityMainBinding
import com.vokilam.findmycar.logger.Logger
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private val locationPermissionRequest = registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
        if (granted) {
            // do nothing
        } else {
            Toast.makeText(this, "Location permission is required", Toast.LENGTH_SHORT).show()
        }
    }

    private val locationSettingsRequest = registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { res ->
        when (res.resultCode) {
            Activity.RESULT_OK -> {}
            Activity.RESULT_CANCELED -> {}
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        findNavController(R.id.nav_host_fragment_content_main).let {
            appBarConfiguration = AppBarConfiguration(it.graph)
            setupActionBarWithNavController(it, appBarConfiguration)
        }

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        when (intent.action) {
            ACTION_GRANT_PERMISSION -> checkLocationPermission()
            ACTION_CHANGE_LOCATION_SETTINGS -> {
                intent.getParcelableExtra<PendingIntent>(EXTRA_RESOLUTION_INTENT)?.let {
                    locationSettingsRequest.launch(IntentSenderRequest.Builder(it).build())
                }
            }
            else -> {}
        }
    }

    private fun checkLocationPermission() {
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        when {
            ContextCompat.checkSelfPermission(
                this,
                permission
            ) == PackageManager.PERMISSION_GRANTED -> { /* do nothing */ }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                permission
            ) -> {
                Toast.makeText(this, "Please grant location permission", Toast.LENGTH_SHORT).show()
            }

            else -> {
                locationPermissionRequest.launch(permission)
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    companion object {
        private val L = Logger()

        const val ACTION_GRANT_PERMISSION = "com.vokilam.findmycar.action.GRANT_PERMISSION"
        const val ACTION_CHANGE_LOCATION_SETTINGS = "com.vokilam.findmycar.action.CHANGE_LOCATION_SETTINGS"
        const val EXTRA_RESOLUTION_INTENT = "resolution"
    }
}