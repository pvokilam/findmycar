package com.vokilam.findmycar.ui.location

import androidx.lifecycle.*
import com.vokilam.findmycar.domain.provider.StaticMapUrlProvider
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.ui.Text
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LocationViewModel @Inject constructor(
    private val appSettings: AppSettings,
    private val debugSettings: DebugSettings,
    private val staticMapUrlProvider: StaticMapUrlProvider
) : ViewModel() {

    private val _locationModel = MutableLiveData<LocationModel>()
    val locationModel: LiveData<LocationModel> = _locationModel

    init {
        viewModelScope.launch {
            appSettings.trackSettingsChanged(AppSettings.KEY_LAST_LOCATION).collect {
                updateLastLocation()
            }
        }
        updateLastLocation()
    }

    private fun updateLastLocation() {
        _locationModel.value = appSettings.lastLocation?.let {
            LocationModel.Location(
                Text.Plain("Your car's location"),
                Text.Plain(it.address),
                Text.Empty,
                staticMapUrlProvider.getLocationPreviewUrl(
                    it.location,
                    debugSettings.previewZoom,
                    debugSettings.previewWidth,
                    debugSettings.previewWidth
                )
            )
        } ?: LocationModel.Empty
    }
}