package com.vokilam.findmycar.ui.settings

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import androidx.preference.*
import com.google.android.material.snackbar.Snackbar
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.AppSettings.Companion.KEY_SELECTED_PAIRED_DEVICE_ADDRESS
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingsFragment : PreferenceFragmentCompat() {

    @Inject lateinit var bluetoothAdapter: BluetoothAdapter
    @Inject lateinit var appSettings: AppSettings

    private var snackbar: Snackbar? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceScreen = preferenceManager.createPreferenceScreen(requireContext()).apply {
            createCategory()
        }
    }

    private fun PreferenceScreen.createCategory() {
        PreferenceCategory(context).apply {
            key = "category_general" // required for managing collapsed state
            title = "General"
            this@createCategory += this // must add category before adding sub preferences

            this += ListPreference(context).apply {
                // TODO: master 28.02.21 update device list when bluetooth is enabled/disabled
                key = KEY_SELECTED_PAIRED_DEVICE_ADDRESS
                title = "Selected paired device (car)"
                // TODO: master 1.03.21 bug: summary is not shown
                summaryProvider = ListPreference.SimpleSummaryProvider.getInstance()
                setOnPreferenceClickListener {
                    if (bluetoothAdapter.isEnabled) {
                        val devices = bluetoothAdapter.bondedDevices.associateBy { it.address }
                        entries = devices.values.map { it.name }.toTypedArray()
                        entryValues = devices.keys.toTypedArray()
                    } else {
                        entries = emptyArray()
                        entryValues = emptyArray()

                        if (snackbar?.isShown != true) {
                            Snackbar.make(
                                listView,
                                "Enable bluetooth",
                                Snackbar.LENGTH_INDEFINITE
                            ).setAction("Enable") {
                                startActivity(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
                            }.also {
                                snackbar = it
                            }.show()
                        }
                    }
                    true
                }
                setOnPreferenceChangeListener { _, newValue ->
                    findPreference<Preference>("reset_$KEY_SELECTED_PAIRED_DEVICE_ADDRESS")
                        ?.isEnabled = (newValue as String).isNotEmpty()
                    true
                }
            }

            this += Preference(context).apply {
                key = "reset_$KEY_SELECTED_PAIRED_DEVICE_ADDRESS"
                title = "Reset selected paired device (car)"
                isEnabled = appSettings.selectedPairedDeviceAddress.isNotEmpty()
                setOnPreferenceClickListener {
                    it.isEnabled = false
                    appSettings.remove(KEY_SELECTED_PAIRED_DEVICE_ADDRESS)
                    findPreference<ListPreference>(KEY_SELECTED_PAIRED_DEVICE_ADDRESS)?.value = ""
                    true
                }
            }
        }
    }
}