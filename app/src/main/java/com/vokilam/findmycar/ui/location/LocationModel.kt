package com.vokilam.findmycar.ui.location

import com.vokilam.findmycar.ui.Text

sealed class LocationModel {
    data class Location(
        val title: Text,
        val subtitle: Text,
        val description: Text,
        val previewUrl: String
    ) : LocationModel()
    object Empty : LocationModel()
}
