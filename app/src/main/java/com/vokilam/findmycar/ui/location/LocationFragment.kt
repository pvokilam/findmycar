package com.vokilam.findmycar.ui.location

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.load
import com.vokilam.findmycar.BuildConfig
import com.vokilam.findmycar.R
import com.vokilam.findmycar.databinding.FragmentCarLocationBinding
import com.vokilam.findmycar.ui.viewBinding
import com.vokilam.findmycar.util.context
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LocationFragment : Fragment(R.layout.fragment_car_location) {

    private val binding by viewBinding(FragmentCarLocationBinding::bind)
    private val viewModel by viewModels<LocationViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        viewModel.locationModel.observe(viewLifecycleOwner, ::updateLocation)
    }

    private fun updateLocation(model: LocationModel) {
        when (model) {
            LocationModel.Empty -> {
                binding.root.displayedChild = CHILD_EMPTY
            }
            is LocationModel.Location -> {
                binding.root.displayedChild = CHILD_LOCATION
                binding.locationCardView.run {
                    title.text = model.title.getString(context)
                    subtitle.text = model.subtitle.getString(context)
                    description.text = model.description.getString(context)
                    preview.load(model.previewUrl)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_car_location, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.menu_debug)?.isVisible = BuildConfig.DEBUG
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_debug -> { findNavController().navigate(R.id.action_FirstFragment_to_DebugSettingsFragment); true }
            R.id.menu_settings -> { findNavController().navigate(R.id.action_FirstFragment_to_SettingsFragment); true }
            else -> false
        }
    }

    companion object {
        private const val CHILD_LOCATION = 0
        private const val CHILD_EMPTY = 1
    }
}