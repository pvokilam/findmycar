package com.vokilam.findmycar.ui.settings

import android.app.NotificationManager
import android.os.Bundle
import androidx.preference.*
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.vokilam.findmycar.domain.Constants
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_ACCURACY
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_STALENESS
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_UPDATES_TIMEOUT
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_HEIGHT
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_WIDTH
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_ZOOM
import com.vokilam.findmycar.ui.notification.NotificationFactory
import com.vokilam.findmycar.work.LocationWorker
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.time.DurationUnit

@AndroidEntryPoint
class DebugSettingsFragment : PreferenceFragmentCompat() {

    @Inject lateinit var debugSettings: DebugSettings
    @Inject lateinit var workManager: WorkManager
    @Inject lateinit var appSettings: AppSettings
    @Inject lateinit var notificationManager: NotificationManager

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceScreen = preferenceManager.createPreferenceScreen(requireContext()).apply {
            createWorkerCategory()
            createLocationAccuracyCategory()
            createLocationPreviewCategory()
        }
    }

    private fun PreferenceScreen.createWorkerCategory() {
        PreferenceCategory(context).apply {
            key = "category_worker" // required for managing collapsed state
            title = "Worker"
            this@createWorkerCategory += this // must add category before adding sub preferences

            this += Preference(context).apply {
                title = "Force launch location worker"
                setOnPreferenceClickListener {
                    val request = OneTimeWorkRequestBuilder<LocationWorker>().build()
                    workManager.enqueueUniqueWork(
                        Constants.WORK_NAME_SAVE_LOCATION,
                        ExistingWorkPolicy.KEEP, request)
                    true
                }
            }

            this += Preference(context).apply {
                title = "Clear last location"
                setOnPreferenceClickListener {
                    appSettings.lastLocation = null
                    notificationManager.cancel(NotificationFactory.NOTIFICATION_ID_LAST_LOCATION)
                    true
                }
            }
        }
    }

    private fun PreferenceScreen.createLocationAccuracyCategory() {
        PreferenceCategory(context).apply {
            key = "category_location_accuracy"
            title = "Location accuracy"
            this@createLocationAccuracyCategory += this

            this += SeekBarPreference(context).apply {
                key = KEY_LOCATION_ACCURACY
                title = "Accuracy (meters)"
                min = 10
                max = 100
                showSeekBarValue = true
                setDefaultValue(debugSettings.locationAccuracy)
            }

            this += SeekBarPreference(context).apply {
                key = KEY_LOCATION_STALENESS
                title = "Staleness (seconds)"
                min = 1
                max = 300
                showSeekBarValue = true
                setDefaultValue(debugSettings.locationStaleness.toInt(DurationUnit.SECONDS))
                tag
            }

            this += SeekBarPreference(context).apply {
                key = KEY_LOCATION_UPDATES_TIMEOUT
                title = "Update timeout (seconds)"
                min = 1
                max = 60
                showSeekBarValue = true
                setDefaultValue(debugSettings.locationUpdatesTimeout.toInt(DurationUnit.SECONDS))
            }

            this += Preference(context).apply {
                title = "Reset to defaults"
                setOnPreferenceClickListener {
                    debugSettings.remove(KEY_LOCATION_ACCURACY)
                    findPreference<SeekBarPreference>(KEY_LOCATION_ACCURACY)?.value =
                        debugSettings.locationAccuracy.toInt()

                    debugSettings.remove(KEY_LOCATION_STALENESS)
                    findPreference<SeekBarPreference>(KEY_LOCATION_STALENESS)?.value =
                        debugSettings.locationStaleness.toInt(DurationUnit.SECONDS)

                    debugSettings.remove(KEY_LOCATION_UPDATES_TIMEOUT)
                    findPreference<SeekBarPreference>(KEY_LOCATION_UPDATES_TIMEOUT)?.value =
                        debugSettings.locationUpdatesTimeout.toInt(DurationUnit.SECONDS)

                    true
                }
            }
        }
    }

    private fun PreferenceScreen.createLocationPreviewCategory() {
        PreferenceCategory(context).apply {
            key = "category_location_preview"
            title = "Location preview"
            this@createLocationPreviewCategory += this

            this += SeekBarPreference(context).apply {
                key = KEY_PREVIEW_ZOOM
                title = "Zoom"
                min = 10
                max = 20
                showSeekBarValue = true
                setDefaultValue(debugSettings.previewZoom)
            }

            this += SeekBarPreference(context).apply {
                key = KEY_PREVIEW_WIDTH
                title = "Width"
                min = 320
                max = 1280
                showSeekBarValue = true
                setDefaultValue(debugSettings.previewWidth)
            }

            this += SeekBarPreference(context).apply {
                key = KEY_PREVIEW_HEIGHT
                title = "Height"
                min = 160
                max = 640
                seekBarIncrement = 16
                showSeekBarValue = true
                setDefaultValue(debugSettings.previewHeight)
            }

            this += Preference(context).apply {
                title = "Reset to defaults"
                setOnPreferenceClickListener {
                    debugSettings.remove(KEY_PREVIEW_ZOOM)
                    findPreference<SeekBarPreference>(KEY_PREVIEW_ZOOM)
                        ?.value = debugSettings.previewZoom

                    debugSettings.remove(KEY_PREVIEW_WIDTH)
                    findPreference<SeekBarPreference>(KEY_PREVIEW_WIDTH)
                        ?.value = debugSettings.previewWidth

                    debugSettings.remove(KEY_PREVIEW_HEIGHT)
                    findPreference<SeekBarPreference>(KEY_PREVIEW_HEIGHT)
                        ?.value = debugSettings.previewHeight

                    true
                }
            }
        }
    }
}