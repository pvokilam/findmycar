package com.vokilam.findmycar.ui.notification

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.app.NotificationCompat
import com.vokilam.findmycar.R
import com.vokilam.findmycar.domain.model.LocationAddress
import com.vokilam.findmycar.ui.MainActivity

object NotificationFactory {

    const val NOTIFICATION_ID_SAVING_LOCATION = 0
    const val NOTIFICATION_ID_LAST_LOCATION = 1
    const val NOTIFICATION_GRANT_PERMISSION = 2
    const val NOTIFICATION_CHANGE_LOCATION_SETTINGS = 3
    const val NOTIFICATION_LOCATION_NOT_FOUND = 4

    fun createSavingLocationNotification(context: Context, cancelIntent: PendingIntent): Notification {
        val title = context.getString(R.string.notif_location_title)
        val cancel = context.getString(android.R.string.cancel)

        return NotificationCompat.Builder(context, NotificationChannel.WORK_IN_PROGRESS.id)
            .setContentTitle(title)
            .setTicker(title)
            .setProgress(0, 0, true)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setOngoing(true)
            .addAction(android.R.drawable.ic_delete, cancel, cancelIntent)
            .build()
    }

    fun createLastSavedLocationNotification(context: Context, location: LocationAddress, preview: Bitmap?): Notification {
        val mapUri = Uri.Builder()
            .scheme("geo")
            .authority("${location.location.latitude},${location.location.longitude}")
            .appendQueryParameter("q", "${location.location.latitude},${location.location.longitude}")
            .build()
        val mapIntent = Intent(Intent.ACTION_VIEW, mapUri)
        val contentIntent = PendingIntent.getActivity(context, 0, mapIntent, 0)

        // TODO: master 25.02.21 add get directions action
        val builder = NotificationCompat.Builder(context, NotificationChannel.UPDATES.id)
            .setContentTitle("Location saved")
            .setContentText(location.address.ifEmpty { "no address" })
            .setTicker("Location saved")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(contentIntent)

        preview?.let { builder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(it)) }
        return builder.build()
    }

    fun createGrantLocationPermissionNotification(context: Context): Notification {
        val grantPermissionIntent = Intent(context, MainActivity::class.java)
            .setAction(MainActivity.ACTION_GRANT_PERMISSION)
        val contentIntent = PendingIntent.getActivity(context, 0, grantPermissionIntent, 0)
        return NotificationCompat.Builder(context, NotificationChannel.WARNINGS.id)
            .setContentTitle("Location permission is not granted")
            .setContentText("Allow use of location")
            .setTicker("Location permission is not granted")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(contentIntent)
            .setAutoCancel(true)
            .build()
    }

    fun createChangeLocationSettingsNotification(context: Context, resolutionIntent: PendingIntent): Notification {
        val changeLocationSettingsIntent = Intent(context, MainActivity::class.java)
            .setAction(MainActivity.ACTION_CHANGE_LOCATION_SETTINGS)
            .putExtra(MainActivity.EXTRA_RESOLUTION_INTENT, resolutionIntent)
        val contentIntent = PendingIntent.getActivity(context, 0, changeLocationSettingsIntent, 0)
        return NotificationCompat.Builder(context, NotificationChannel.WARNINGS.id)
            .setContentTitle("Insufficient location settings")
            .setContentText("Change location settings")
            .setTicker("Insufficient location settings")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(contentIntent)
            .setAutoCancel(true)
            .build()
    }

    fun createLocationNotFoundNotification(context: Context): Notification {
        return NotificationCompat.Builder(context, NotificationChannel.WARNINGS.id)
            .setContentTitle("Location not found")
            .setContentText("Location not found")
            .setTicker("Location not found")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setAutoCancel(true)
            .build()
    }
}