package com.vokilam.findmycar.ui.notification

import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import com.vokilam.findmycar.R

enum class NotificationChannel(
    val id: String,
    private val nameResId: Int,
    private val importance: Int,
    private val config: android.app.NotificationChannel.() -> Unit = {}
) {
    WORK_IN_PROGRESS(
        "wip",
        R.string.notif_wip,
        NotificationManagerCompat.IMPORTANCE_LOW
    ),
    UPDATES(
        "updates",
        R.string.notif_updates,
        NotificationManagerCompat.IMPORTANCE_MIN,
        @RequiresApi(Build.VERSION_CODES.O)
        { setShowBadge(false) }
    ),
    WARNINGS(
        "warnings",
        R.string.notif_warnings,
        NotificationManagerCompat.IMPORTANCE_DEFAULT
    );

    @RequiresApi(Build.VERSION_CODES.O)
    fun createChannel(context: Context, notificationManager: NotificationManager) {
        val name = context.getString(nameResId)
        val channel = android.app.NotificationChannel(id, name, importance).apply(config)
        notificationManager.createNotificationChannel(channel)
    }

    companion object {
        fun createAllChannels(context: Context, notificationManager: NotificationManager) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                values().forEach { it.createChannel(context, notificationManager) }
            }
        }
    }
}