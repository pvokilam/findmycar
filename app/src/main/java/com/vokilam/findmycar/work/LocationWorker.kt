package com.vokilam.findmycar.work

import android.app.NotificationManager
import android.content.Context
import android.content.pm.ServiceInfo
import android.graphics.Bitmap
import android.os.Build
import androidx.core.graphics.drawable.toBitmap
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import coil.ImageLoader
import coil.request.ImageRequest
import com.google.android.gms.common.api.ResolvableApiException
import com.vokilam.findmycar.domain.usecase.GetLocationUseCase
import com.vokilam.findmycar.domain.Result.*
import com.vokilam.findmycar.domain.provider.StaticMapUrlProvider
import com.vokilam.findmycar.domain.exception.InsufficientLocationSettingsException
import com.vokilam.findmycar.domain.exception.LocationNotFoundException
import com.vokilam.findmycar.domain.exception.PermissionDeniedException
import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.logger.Logger
import com.vokilam.findmycar.ui.notification.NotificationFactory
import com.vokilam.findmycar.ui.notification.NotificationFactory.NOTIFICATION_CHANGE_LOCATION_SETTINGS
import com.vokilam.findmycar.ui.notification.NotificationFactory.NOTIFICATION_GRANT_PERMISSION
import com.vokilam.findmycar.ui.notification.NotificationFactory.NOTIFICATION_ID_LAST_LOCATION
import com.vokilam.findmycar.ui.notification.NotificationFactory.NOTIFICATION_ID_SAVING_LOCATION
import com.vokilam.findmycar.ui.notification.NotificationFactory.NOTIFICATION_LOCATION_NOT_FOUND
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

@HiltWorker
class LocationWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val workManager: WorkManager,
    private val getLocationUseCase: GetLocationUseCase,
    private val appSettings: AppSettings,
    private val debugSettings: DebugSettings,
    private val notificationFactory: NotificationFactory,
    private val notificationManager: NotificationManager,
    private val staticMapUrlProvider: StaticMapUrlProvider,
    private val imageLoader: ImageLoader
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        L.debug { "doWork(): params=$inputData" }
        setForeground(createForegroundInfo())

        when (val res = getLocationUseCase()) {
            is Success -> {
                L.info { "doWork(): location=$res" }
                val location = res.data.location
                val preview = getLocationPreview(location)

                appSettings.lastLocation = res.data

                // TODO: master 27.12.21 update static image according to day-night theme
                notificationManager.notify(
                    NOTIFICATION_ID_LAST_LOCATION,
                    notificationFactory.createLastSavedLocationNotification(
                        applicationContext,
                        res.data,
                        preview
                    )
                )
            }
            is Failure -> {
                when (res.error) {
                    is PermissionDeniedException -> {
                        notificationManager.notify(
                            NOTIFICATION_GRANT_PERMISSION,
                            notificationFactory.createGrantLocationPermissionNotification(applicationContext)
                        )
                    }
                    is InsufficientLocationSettingsException -> {
                        val contentIntent = (res.error.cause as ResolvableApiException).resolution
                        notificationManager.notify(
                            NOTIFICATION_CHANGE_LOCATION_SETTINGS,
                            notificationFactory.createChangeLocationSettingsNotification(applicationContext, contentIntent)
                        )
                    }
                    is LocationNotFoundException -> {
                        appSettings.lastLocation = null
                        notificationManager.notify(
                            NOTIFICATION_LOCATION_NOT_FOUND,
                            notificationFactory.createLocationNotFoundNotification(applicationContext)
                        )
                    }
                    else -> L.error(res.error) { "doWork(): error=$res" }
                }
            }
        }
        // TODO: master 3.03.21 foreground notification is not dismissed on S20
        return Result.success()
    }

    private suspend fun getLocationPreview(location: Location): Bitmap? {
        val url = staticMapUrlProvider.getLocationPreviewUrl(
            location,
            debugSettings.previewZoom,
            debugSettings.previewWidth,
            debugSettings.previewHeight
        )
        L.debug { "getLocationPreview(): url=$url" }

        return runCatching {
            val request = ImageRequest.Builder(applicationContext).data(url).build()
            imageLoader.execute(request).drawable?.toBitmap()
        }.onFailure {
            L.error { "getLocationPreview(): failed with $it" }
        }.getOrNull()
    }

    private fun createForegroundInfo(): ForegroundInfo {
        val cancelIntent = workManager.createCancelPendingIntent(id)
        val notification = notificationFactory.createSavingLocationNotification(applicationContext, cancelIntent)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ForegroundInfo(NOTIFICATION_ID_SAVING_LOCATION, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION)
        } else {
            ForegroundInfo(NOTIFICATION_ID_SAVING_LOCATION, notification)
        }
    }

    companion object {
        private val L = Logger()
    }
}