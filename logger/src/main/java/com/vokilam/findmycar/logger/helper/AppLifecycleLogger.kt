package com.vokilam.findmycar.logger.helper

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.vokilam.findmycar.logger.Logger

internal object AppLifecycleLogger : DefaultLifecycleObserver {
    private val L = Logger()

    override fun onStart(owner: LifecycleOwner) {
        L.verb { "onMoveToForeground()" }
    }

    override fun onStop(owner: LifecycleOwner) {
        L.verb { "onMoveToBackground()" }
    }
}
