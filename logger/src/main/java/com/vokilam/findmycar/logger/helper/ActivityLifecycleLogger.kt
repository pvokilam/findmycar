package com.vokilam.findmycar.logger.helper

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.vokilam.findmycar.logger.Logger

internal object ActivityLifecycleLogger : Application.ActivityLifecycleCallbacks {
    private val L = Logger()

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        L.verb { "onActivityCreated(): $p0, bundle=$p1" }

        if (p0 is FragmentActivity) {
            p0.supportFragmentManager.registerFragmentLifecycleCallbacks(FragmentLifecycleLogger, true)
        }
    }

    override fun onActivityStarted(p0: Activity) {
        L.verb { "onActivityStarted(): $p0" }
    }

    override fun onActivityResumed(p0: Activity) {
        L.verb { "onActivityResumed(): $p0" }
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        L.verb { "onActivitySaveInstanceState(): $p0, bundle=$p1" }
    }

    override fun onActivityPaused(p0: Activity) {
        L.verb { "onActivityPaused(): $p0" }
    }

    override fun onActivityStopped(p0: Activity) {
        L.verb { "onActivityStopped(): $p0" }
    }

    override fun onActivityDestroyed(p0: Activity) {
        L.verb { "onActivityDestroyed(): $p0" }
    }
}
