package com.vokilam.findmycar.logger

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.preference.PreferenceManager
import com.vokilam.findmycar.logger.helper.ActivityLifecycleLogger
import com.vokilam.findmycar.logger.helper.AppLifecycleLogger
import com.vokilam.findmycar.logger.helper.ComponentCallbacksLogger
import com.vokilam.findmycar.logger.helper.SharedPreferencesLogger
import com.vokilam.findmycar.logger.timber.FileTree
import com.vokilam.findmycar.logger.timber.LogCatTree
import timber.log.Timber
import java.io.Closeable
import java.io.Flushable

class Logger private constructor(val tag: String) {

    inline fun verb(message: () -> String) = log(Log.VERBOSE, tag, null, message)
    inline fun debug(message: () -> String) = log(Log.DEBUG, tag, null, message)
    inline fun info(message: () -> String) = log(Log.INFO, tag, null, message)
    inline fun warn(message: () -> String) = log(Log.WARN, tag, null, message)
    inline fun error(message: () -> String) = log(Log.ERROR, tag, null, message)
    inline fun error(tr: Throwable? = null, message: () -> String) = log(Log.ERROR, tag, tr, message)

    inline fun log(priority: Int, tag: String, tr: Throwable?, message: () -> String) {
        // In combination with proguard, condition on statically resolved BuildConfig flag removes `StringBuilder`
        // instances when string interpolation is used in message block
        if (BuildConfig.DEBUG) {
            val msg = "[${Thread.currentThread().name}] ${message()}"
            Timber.tag(tag)
            Timber.log(priority, tr, msg)
        }
    }

    companion object {
        private const val MAX_TAG_LENGTH = 23
        private val ANONYMOUS_CLASS = Regex("(\\$\\d+)+$")

        // TODO: master 25.12.21 init via androidx.startup automatically
        fun init(app: Application) {
            Thread.setDefaultUncaughtExceptionHandler(
                LoggerExceptionHandler(Thread.getDefaultUncaughtExceptionHandler())
            )

            Timber.plant(LogCatTree())
            app.getExternalFilesDir("logs")?.let {
                Timber.plant(FileTree(it.absolutePath))
            }

            app.registerActivityLifecycleCallbacks(ActivityLifecycleLogger)
            app.registerComponentCallbacks(ComponentCallbacksLogger)
            ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleLogger)
            PreferenceManager.getDefaultSharedPreferences(app)
                .registerOnSharedPreferenceChangeListener(SharedPreferencesLogger)
        }

        operator fun invoke(tag: String = createTag()): Logger {
            return Logger(tag)
        }

        private fun createTag(): String {
            val stackTrace = Throwable().stackTrace
            val element = stackTrace.first { it.className != Companion::class.java.name }
            return createStackElementTag(element)
        }

        private fun createStackElementTag(element: StackTraceElement): String {
            val tag = ANONYMOUS_CLASS.replace(element.className.substringAfterLast('.'), "")

            // Tag length limit was removed in API 24.
            return if (tag.length <= MAX_TAG_LENGTH || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tag
            } else {
                tag.substring(0, MAX_TAG_LENGTH)
            }
        }

        fun flush() {
            Timber.forest()
                .filterIsInstance<Flushable>()
                .forEach(Flushable::flush)
        }

        fun close() {
            Timber.forest()
                .filterIsInstance<Closeable>()
                .forEach(Closeable::close)
        }
    }
}