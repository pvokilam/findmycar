package com.vokilam.findmycar.logger.helper

import android.content.SharedPreferences
import com.vokilam.findmycar.logger.Logger

internal object SharedPreferencesLogger : SharedPreferences.OnSharedPreferenceChangeListener {
    private val L = Logger()

    override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String) {
        L.verb { "onSharedPreferenceChanged(): $key=${prefs.all[key]}" }
    }
}