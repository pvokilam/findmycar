package com.vokilam.findmycar.domain.usecase

import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.provider.SystemClockProvider
import com.vokilam.findmycar.domain.settings.DebugSettings
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class LocationAccuracyCheckerTest {

    private lateinit var checker: LocationAccuracyChecker

    @MockK
    private lateinit var systemClockProvider: SystemClockProvider
    @MockK
    private lateinit var debugSettings: DebugSettings

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        checker = LocationAccuracyChecker(systemClockProvider, debugSettings)
    }

    @ParameterizedTest
    @MethodSource("checkerArguments")
    internal fun testIsSatisfied(location: Location?, expected: Boolean) {
        every { systemClockProvider.elapsedRealtime() } returns ELAPSED_REALTIME
        every { debugSettings.locationStaleness } returns STALENESS
        every { debugSettings.locationAccuracy } returns ACCURACY.toInt()

        assertEquals(expected, checker.isSatisfied(location))
    }

    companion object {
        private const val ACCURACY = 10f
        private val STALENESS = 10.toDuration(DurationUnit.SECONDS)
        private val ELAPSED_REALTIME = 10.toDuration(DurationUnit.SECONDS)

        private val LOCATION = Location(22.22, 33.33, 10f, 5.toDuration(DurationUnit.SECONDS))

        @JvmStatic
        fun checkerArguments() = arrayOf(
            // location, expected
            arrayOf(null, false),
            arrayOf(LOCATION.copy(accuracy = ACCURACY, elapsedRealtime = ELAPSED_REALTIME), true),
            arrayOf(LOCATION.copy(accuracy = ACCURACY.inc(), elapsedRealtime = ELAPSED_REALTIME), false),
            arrayOf(LOCATION.copy(accuracy = ACCURACY, elapsedRealtime = ELAPSED_REALTIME - STALENESS), false),
            arrayOf(LOCATION.copy(accuracy = ACCURACY.inc(), elapsedRealtime = ELAPSED_REALTIME + STALENESS), false),
        )
    }
}