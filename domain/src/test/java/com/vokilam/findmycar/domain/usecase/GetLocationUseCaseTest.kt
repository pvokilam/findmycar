package com.vokilam.findmycar.domain.usecase

import com.vokilam.findmycar.domain.Result
import com.vokilam.findmycar.domain.exception.LocationNotFoundException
import com.vokilam.findmycar.domain.interactor.GeocoderInteractor
import com.vokilam.findmycar.domain.interactor.LocationInteractor
import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.model.LocationAddress
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.test.TestCoroutineExtension
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.junit.runners.Parameterized
import kotlin.time.DurationUnit
import kotlin.time.toDuration

@ExperimentalCoroutinesApi
@ExtendWith(TestCoroutineExtension::class)
internal class GetLocationUseCaseTest {

    private lateinit var useCase: GetLocationUseCase

    @MockK
    private lateinit var locationInteractor: LocationInteractor
    @MockK
    private lateinit var geocoderInteractor: GeocoderInteractor
    @MockK
    private lateinit var locationAccuracyChecker: LocationAccuracyChecker
    @MockK
    private lateinit var debugSettings: DebugSettings

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        useCase = GetLocationUseCase(locationInteractor, geocoderInteractor, locationAccuracyChecker, debugSettings)
    }

    @ParameterizedTest
    @MethodSource("locationSatisfiedArgs")
    internal fun `when location is accurate should return location`(
        lastSatisfied: Boolean,
        currentSatisfied: Boolean,
        updatedSatisfied: Boolean,
        expectedLocation: Location?
    ) = runTest {
        coEvery { locationInteractor.getLastLocation() } returns LAST_LOCATION
        coEvery { locationInteractor.getCurrentLocation() } returns CURRENT_LOCATION
        coEvery { locationInteractor.getLocationUpdates() } returns flowOf(UPDATED_LOCATION)

        every { locationAccuracyChecker.isSatisfied(LAST_LOCATION) } returns lastSatisfied
        every { locationAccuracyChecker.isSatisfied(CURRENT_LOCATION) } returns currentSatisfied
        every { locationAccuracyChecker.isSatisfied(UPDATED_LOCATION) } returns updatedSatisfied

        every { debugSettings.locationUpdatesTimeout } returns 1.toDuration(DurationUnit.MILLISECONDS)
        coEvery { geocoderInteractor.getFromLocation(any(), any()) } returns ADDRESS

        val actual = useCase() as Result.Success
        assertEquals(expectedLocation, actual.data.location)
    }

    @Test
    internal fun `when location is not null should geocode address`() = runTest {
        coEvery { locationInteractor.getLastLocation() } returns LAST_LOCATION
        every { locationAccuracyChecker.isSatisfied(LAST_LOCATION) } returns true
        coEvery { geocoderInteractor.getFromLocation(any(), any()) } returns ADDRESS

        val actual = useCase() as Result.Success
        assertEquals(ADDRESS, actual.data.address)
    }

    @Test
    internal fun `when location is null should throw exception`() = runTest {
        coEvery { locationInteractor.getLastLocation() } returns null
        every { locationAccuracyChecker.isSatisfied(null) } returns true

        val actual = useCase() as Result.Failure
        assertTrue(actual.error is LocationNotFoundException)
    }

    companion object {
        private val LAST_LOCATION = Location(22.33, 33.22, 0.1f, 15.toDuration(DurationUnit.SECONDS))
        private val CURRENT_LOCATION = Location(33.32, 32.33, 0.09f, 10.toDuration(DurationUnit.SECONDS))
        private val UPDATED_LOCATION = Location(44.34, 34.44, 0.1f, 5.toDuration(DurationUnit.SECONDS))
        private const val ADDRESS = "address"

        @JvmStatic
        fun locationSatisfiedArgs() = arrayOf(
            // lastSatisfied, currentSatisfied, updatedSatisfied, expectedLocation
            arrayOf(true, false, false, LAST_LOCATION),
            arrayOf(false, true, false, CURRENT_LOCATION),
            arrayOf(false, false, true, UPDATED_LOCATION),
        )
    }
}