package com.vokilam.findmycar.domain.interactor

import com.vokilam.findmycar.domain.model.Location
import kotlinx.coroutines.flow.Flow

interface LocationInteractor {
    suspend fun getLastLocation(): Location?
    suspend fun getCurrentLocation(): Location?
    suspend fun getLocationUpdates(): Flow<Location>
}