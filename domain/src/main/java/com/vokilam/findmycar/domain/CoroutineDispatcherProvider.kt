package com.vokilam.findmycar.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject

data class CoroutineDispatcherProvider(
    val main: CoroutineDispatcher,
    val default: CoroutineDispatcher,
    val io: CoroutineDispatcher
) {
    @Inject
    constructor() : this(Main, Default, IO)
    constructor(sharedDispatcher: CoroutineDispatcher) : this(sharedDispatcher, sharedDispatcher, sharedDispatcher)
}
