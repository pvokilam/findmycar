package com.vokilam.findmycar.domain.provider

import com.vokilam.findmycar.domain.model.Location

interface StaticMapUrlProvider {
    fun getLocationPreviewUrl(
        location: Location,
        zoom: Int,
        width: Int,
        height: Int
    ): String
}