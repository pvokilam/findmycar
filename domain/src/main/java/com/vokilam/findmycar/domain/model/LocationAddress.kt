package com.vokilam.findmycar.domain.model

data class LocationAddress(val location: Location, val address: String)
