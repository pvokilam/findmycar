package com.vokilam.findmycar.domain.interactor

interface GeocoderInteractor {
    suspend fun getFromLocation(latitude: Double, longitude: Double): String
}