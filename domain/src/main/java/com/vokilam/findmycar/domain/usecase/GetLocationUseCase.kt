package com.vokilam.findmycar.domain.usecase

import com.vokilam.findmycar.domain.interactor.GeocoderInteractor
import com.vokilam.findmycar.domain.interactor.LocationInteractor
import com.vokilam.findmycar.domain.provider.SystemClockProvider
import com.vokilam.findmycar.domain.exception.LocationNotFoundException
import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.model.LocationAddress
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.logger.Logger
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withTimeout
import javax.inject.Inject

class GetLocationUseCase @Inject constructor(
    private val locationInteractor: LocationInteractor,
    private val geocoderInteractor: GeocoderInteractor,
    private val locationAccuracyChecker: LocationAccuracyChecker,
    private val debugSettings: DebugSettings
) : UseCase<LocationAddress> {

    override suspend fun execute(): LocationAddress {
        L.info { "execute()" }

        val lastLocation = locationInteractor.getLastLocation()
        L.debug { "execute(): lastLocation=$lastLocation" }

        val location = if (!locationAccuracyChecker.isSatisfied(lastLocation)) {
            val currentLocation = locationInteractor.getCurrentLocation()
            L.debug { "execute(): currentLocation=$currentLocation" }

            if (!locationAccuracyChecker.isSatisfied(currentLocation)) {
                val updatedLocation = withTimeout(debugSettings.locationUpdatesTimeout) {
                    locationInteractor.getLocationUpdates()
                        .onEach { L.verb { "getLocationUpdates(): new update, location=$it" } }
                        .onCompletion { L.debug { "getLocationUpdates(): collection completed, cause=$it" } }
                        .firstOrNull(locationAccuracyChecker::isSatisfied) // TODO: master 25.02.21 choose the best
                }
                L.debug { "execute(): updatedLocation=$updatedLocation" }

                updatedLocation ?: currentLocation ?: lastLocation
            } else {
                currentLocation
            }
        } else {
            lastLocation
        }

        return if (location != null) {
            val address = geocoderInteractor.getFromLocation(location.latitude, location.longitude)
            LocationAddress(location, address)
        } else {
            throw LocationNotFoundException()
        }
    }

    companion object {
        private val L = Logger()
    }
}

class LocationAccuracyChecker @Inject constructor(
    private val systemClockProvider: SystemClockProvider,
    private val debugSettings: DebugSettings
) {
    fun isSatisfied(location: Location?): Boolean {
        if (location == null) return false

        val staleness = systemClockProvider.elapsedRealtime() - location.elapsedRealtime
        val accuracy = location.accuracy

        L.verb { "isLocationAccuracySatisfied(): staleness=$staleness, accuracy=$accuracy" }
        return staleness < debugSettings.locationStaleness && accuracy <= debugSettings.locationAccuracy
    }

    companion object {
        private val L = Logger()
    }
}