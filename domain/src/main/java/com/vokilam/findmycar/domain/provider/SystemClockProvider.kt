package com.vokilam.findmycar.domain.provider

import kotlin.time.Duration

interface SystemClockProvider {
    fun elapsedRealtime(): Duration
}