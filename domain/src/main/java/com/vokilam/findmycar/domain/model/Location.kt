package com.vokilam.findmycar.domain.model

import kotlin.time.Duration

data class Location(
    val latitude: Double,
    val longitude: Double,
    val accuracy: Float,
    val elapsedRealtime: Duration
)