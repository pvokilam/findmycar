package com.vokilam.findmycar.domain

object Constants {
    // TODO: master 15.12.21 find better place
    const val WORK_NAME_SAVE_LOCATION = "save_location"
    const val LOCATION_PREVIEW_WIDTH = 480
    const val LOCATION_PREVIEW_HEIGHT = 220
    const val LOCATION_PREVIEW_ZOOM = 17
}