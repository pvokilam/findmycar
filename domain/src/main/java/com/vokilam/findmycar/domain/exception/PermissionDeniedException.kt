package com.vokilam.findmycar.domain.exception

class PermissionDeniedException : Exception()