package com.vokilam.findmycar.domain.exception

class LocationNotFoundException : Exception()