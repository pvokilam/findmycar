package com.vokilam.findmycar.domain.settings

import kotlin.time.Duration

interface DebugSettings : Settings {
    var locationAccuracy: Int
    var locationStaleness: Duration
    var locationUpdatesTimeout: Duration

    var previewZoom: Int
    var previewWidth: Int
    var previewHeight: Int

    companion object {
        const val KEY_LOCATION_ACCURACY = "location_accuracy"
        const val KEY_LOCATION_STALENESS = "location_staleness"
        const val KEY_LOCATION_UPDATES_TIMEOUT = "location_updates_timeout"

        const val KEY_PREVIEW_ZOOM = "preview_zoom"
        const val KEY_PREVIEW_WIDTH = "preview_width"
        const val KEY_PREVIEW_HEIGHT = "preview_height"
    }
}