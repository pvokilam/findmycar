package com.vokilam.findmycar.domain.usecase

import com.vokilam.findmycar.domain.Result

interface UseCase<out T> {
    suspend fun execute(): T

    suspend operator fun invoke(): Result<T> {
        return try {
            Result.Success(execute())
        } catch (e: Exception) {
            Result.Failure(e)
        }
    }
}