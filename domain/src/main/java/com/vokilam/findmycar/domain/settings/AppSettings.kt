package com.vokilam.findmycar.domain.settings

import com.vokilam.findmycar.domain.model.LocationAddress

interface AppSettings : Settings {
    var selectedPairedDeviceAddress: String
    var lastLocation: LocationAddress?

    companion object {
        const val KEY_SELECTED_PAIRED_DEVICE_ADDRESS = "selected_paired_device_address"
        const val KEY_LAST_LOCATION = "last_location"
    }
}