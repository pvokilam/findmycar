package com.vokilam.findmycar.domain.exception

class InsufficientLocationSettingsException(cause: Throwable?) : Exception(cause)