package com.vokilam.analytics

import com.vokilam.analytics.core.Tracker
import com.vokilam.analytics.core.builder.EventBuilder
import com.vokilam.analytics.core.builder.event
import com.vokilam.analytics.core.model.EventMapping
import com.vokilam.analytics.core.model.UserProperty
import com.vokilam.findmycar.logger.Logger
import javax.inject.Inject
import kotlin.properties.Delegates

class Analytics @Inject constructor(
    trackerSet: Set<@JvmSuppressWildcards Tracker>
) {
    private val trackers = trackerSet.associateBy { it.key }

    var isEnabled: Boolean by Delegates.observable(true) { _, _, newValue ->
        trackers.values.forEach {
            it.isEnabled = newValue
        }
    }

    operator fun get(key: Tracker.Key<*>): Tracker? {
        return trackers[key]
    }

    fun setUserId(userId: String?) {
        L.info { "setUserId(): userId=$userId" }
        trackers.values.forEach {
            it.setUserId(userId)
        }
    }

    fun trackEvent(event: EventMapping) {
        L.info { "trackEvent(): event=$event" }
        if (!shouldTrack(event)) return
        event.mappings.forEach { (key, event) ->
            trackers[key]?.trackEvent(event)
        }
    }

    private fun shouldTrack(event: EventMapping): Boolean {
        return isEnabled && event.raw.isEnabled
    }

    fun trackEvent(init: EventBuilder.() -> Unit) {
        trackEvent(event(init))
    }

    fun trackUserProperty(property: UserProperty) {
        TODO("Not yet implemented")
    }

    companion object {
        private val L = Logger()
    }
}