package com.vokilam.analytics.core.model

import com.vokilam.analytics.core.Tracker

data class UserProperty(
    val key: String,
    val operation: Op,
    val isEnabled: Boolean = true
) {
    sealed class Op {
        data class Set(val value: Any) : Op()
        object Unset : Op()
    }
}

data class UserPropertyMapping(
    val raw: UserProperty,
    val mappings: Map<Tracker.Key<*>, UserProperty> = mapOf()
) {
    operator fun get(key: Tracker.Key<*>): UserProperty? {
        return mappings[key]
    }

    override fun toString(): String {
        return "UserPropertyMapping(property=$raw, trackers=${mappings.keys})"
    }
}