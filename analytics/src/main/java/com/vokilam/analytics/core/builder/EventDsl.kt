package com.vokilam.analytics.core.builder

import com.vokilam.analytics.core.Tracker
import com.vokilam.analytics.core.model.Event
import com.vokilam.analytics.core.model.EventMapping

@DslMarker
annotation class EventDsl

fun event(init: EventBuilder.() -> Unit): EventMapping {
    return EventBuilder().apply(init).build()
}

@EventDsl
class EventBuilder {
    lateinit var name: String
    var enabled: Boolean = true

    private var properties = mutableMapOf<String, Any>()
    private var mappings = mapOf<Tracker.Key<*>, Event>()

    fun putProperties(props: Map<String, Any?>) {
        props
            .filterValues { it != null }
            .mapValuesTo(properties) { (key, value) ->
                validateProperty(key, value as Any)
            }
    }

    fun putProperties(vararg pairs: Pair<String, Any?>) {
        putProperties(pairs.toMap())
    }

    fun mappings(init: EventMappingsBuilder.() -> Unit) {
        mappings = EventMappingsBuilder(Event(name, properties, enabled)).apply(init).build()
    }

    fun build(): EventMapping {
        return EventMapping(Event(name, properties, enabled), mappings)
    }
}

@EventDsl
class EventMappingsBuilder(private val rawEvent: Event) {
    private val mappings = mutableMapOf<Tracker.Key<*>, Event>()

    fun mapping(trackerKey: Tracker.Key<*>, init: EventMappingBuilder.() -> Unit) {
        mappings[trackerKey] = EventMappingBuilder(rawEvent).apply(init).build()
    }

    fun build(): Map<Tracker.Key<*>, Event> {
        return mappings
    }
}

@EventDsl
class EventMappingBuilder(private val rawEvent: Event) {
    var name: String = rawEvent.name
    var enabled: Boolean = rawEvent.isEnabled
    private val keys = mutableMapOf<String, String>()
    private val values = mutableMapOf<String, Any>()

    fun addAllKeys() {
        keys.putAll(rawEvent.properties.keys.map { Pair(it, it) })
    }

    fun key(originalKey: String, newKey: String) {
        check(rawEvent.properties.containsKey(originalKey)) { "'$originalKey' must be a property key" }
        keys[originalKey] = newKey
    }

    fun value(originalKey: String, newValue: Any) {
        check(rawEvent.properties.containsKey(originalKey)) { "'$originalKey' must be a property key" }
        values[originalKey] = validateProperty(originalKey, newValue)
    }

    fun <T : Any> transformValue(originalKey: String, transform: (T) -> Any) {
        check(rawEvent.properties.containsKey(originalKey)) { "'$originalKey' must be a property key" }
        @Suppress("UNCHECKED_CAST")
        values[originalKey] = validateProperty(
            originalKey,
            transform(rawEvent.properties[originalKey] as T)
        )
    }

    operator fun String.unaryPlus() {
        check(rawEvent.properties.contains(this)) { "'$this' must be a property key" }
        keys[this] = this
    }

    operator fun String.unaryMinus() {
        check(rawEvent.properties.contains(this)) { "'$this' must be a property key" }
        keys.remove(this)
    }

    fun build(): Event {
        val properties = rawEvent.properties
            .filterKeys { keys.containsKey(it) }
            .mapKeys {
                keys[it.key] ?: error("key '${it.key}' is not found")
            }
            .mapValues {
                values[it.key] ?: it.value
            }
        return Event(name, properties, enabled)
    }
}
