package com.vokilam.analytics.core.model

import com.vokilam.analytics.core.Tracker

data class Event(
    val name: String,
    val properties: Map<String, Any> = emptyMap(),
    val isEnabled: Boolean = true
)

data class EventMapping(
    val raw: Event,
    val mappings: Map<Tracker.Key<*>, Event> = mapOf()
) {
    operator fun get(key: Tracker.Key<*>): Event? {
        return mappings[key]
    }

    override fun toString(): String {
        return "EventMapping(event=$raw, trackers=${mappings.keys})"
    }
}