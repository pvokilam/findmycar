package com.vokilam.analytics.core

open class NamedKey<T : Tracker>(private val name: String) : Tracker.Key<T> {
    override fun toString(): String = name
}