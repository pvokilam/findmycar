package com.vokilam.analytics.core.builder

import com.vokilam.analytics.core.Tracker
import com.vokilam.analytics.core.model.UserProperty
import com.vokilam.analytics.core.model.UserProperty.Op
import com.vokilam.analytics.core.model.UserPropertyMapping

@DslMarker
annotation class UserPropertyDsl

fun userProperty(init: UserPropertyBuilder.() -> Unit): UserPropertyMapping {
    return UserPropertyBuilder().apply(init).build()
}

@UserPropertyDsl
class UserPropertyBuilder {
    lateinit var key: String
    lateinit var operation: Op
    var enabled: Boolean = true

    private var mappings = mapOf<Tracker.Key<*>, UserProperty>()

    fun mappings(init: UserPropertyMappingsBuilder.() -> Unit) {
        mappings = UserPropertyMappingsBuilder(UserProperty(key, operation, enabled)).apply(init).build()
    }

    fun build(): UserPropertyMapping {
        return UserPropertyMapping(UserProperty(key, operation, enabled), mappings)
    }
}

@UserPropertyDsl
class UserPropertyMappingsBuilder(private val rawProperty: UserProperty) {
    private val mappings = mutableMapOf<Tracker.Key<*>, UserProperty>()

    fun mapping(trackerKey: Tracker.Key<*>, init: UserPropertyMappingBuilder.() -> Unit) {
        mappings[trackerKey] = UserPropertyMappingBuilder(rawProperty).apply(init).build()
    }

    fun build(): Map<Tracker.Key<*>, UserProperty> {
        return mappings
    }
}

@UserPropertyDsl
class UserPropertyMappingBuilder(private val rawProperty: UserProperty) {
    var key: String = rawProperty.key
    private var operation: Op = rawProperty.operation
    var enabled: Boolean = rawProperty.isEnabled

    fun value(newValue: Any) {
        operation = when (val v = operation) {
            is Op.Set -> v.copy(value = validateProperty(rawProperty.key, newValue))
            else -> error("`value()` must be called only on a value operation")
        }
    }

    fun <T : Any> transformValue(transform: (T) -> Any) {
        @Suppress("UNCHECKED_CAST")
        operation = when (val v = operation) {
            is Op.Set -> v.copy(value = validateProperty(rawProperty.key, transform(operation as T)))
            else -> error("`transformValue()` must be called only on a value operation")
        }
    }

    fun build(): UserProperty {
        return UserProperty(key, operation, enabled)
    }
}
