package com.vokilam.analytics.core

import com.vokilam.analytics.core.model.Event
import com.vokilam.analytics.core.model.UserProperty

interface Tracker {
    val key: Key<*>
    var isEnabled: Boolean

    fun setUserId(userId: String?)
    fun trackEvent(event: Event)
    fun trackUserProperty(property: UserProperty)

    fun shouldTrack(event: Event): Boolean {
        return isEnabled && event.isEnabled
    }

    fun shouldTrack(property: UserProperty): Boolean {
        return isEnabled && property.isEnabled
    }

    interface Key<T : Tracker>
}