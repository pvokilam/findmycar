package com.vokilam.analytics.core.builder

import com.vokilam.analytics.core.Property

fun validateProperty(key: String, value: Any): Any {
    return when (value) {
        is String, is Int, is Long, is Boolean, is Float, is Double -> value
        is Property -> validateProperty(key, value.value)
        else -> error("value of type ${value::class.simpleName} is not supported, key=${key}")
    }
}