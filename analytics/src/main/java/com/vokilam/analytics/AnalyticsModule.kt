package com.vokilam.analytics

import com.vokilam.analytics.core.Tracker
import com.vokilam.analytics.tracker.dummy.DummyTracker
import com.vokilam.analytics.tracker.dummy.DummyTrackerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet

@Module
@InstallIn(SingletonComponent::class)
abstract class AnalyticsModule {

    @Binds
    abstract fun bindDummyTracker(impl: DummyTrackerImpl): DummyTracker

    @Binds
    @IntoSet
    abstract fun contributeDummyTracker(dummyTracker: DummyTracker): Tracker
}