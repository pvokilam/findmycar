package com.vokilam.analytics.tracker.dummy

import com.vokilam.analytics.core.NamedKey
import com.vokilam.analytics.core.Tracker
import com.vokilam.analytics.core.builder.EventMappingBuilder
import com.vokilam.analytics.core.builder.EventMappingsBuilder

interface DummyTracker : Tracker {
    override val key: Tracker.Key<*> get() = Key
    companion object Key : NamedKey<DummyTracker>("dummy")
}

fun EventMappingsBuilder.dummy(init: EventMappingBuilder.() -> Unit) {
    mapping(DummyTracker, init)
}