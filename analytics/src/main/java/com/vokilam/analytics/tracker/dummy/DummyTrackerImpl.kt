package com.vokilam.analytics.tracker.dummy

import com.vokilam.analytics.core.model.Event
import com.vokilam.analytics.core.model.UserProperty
import com.vokilam.findmycar.logger.Logger
import javax.inject.Inject

class DummyTrackerImpl @Inject constructor() : DummyTracker {

    override var isEnabled: Boolean = true

    override fun setUserId(userId: String?) {
        L.debug { "setUserId(): userId=$userId" }
    }

    override fun trackEvent(event: Event) {
        if (!shouldTrack(event)) return
        L.debug { "trackEvent(): event=$event" }
    }

    override fun trackUserProperty(property: UserProperty) {
        if (!shouldTrack(property)) return
        L.debug { "trackUserProperty(): property=$property" }
    }

    companion object {
        private val L = Logger()
    }
}