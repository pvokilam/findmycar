package com.vokilam.findmycar.data

import android.net.Uri
import com.vokilam.findmycar.domain.provider.StaticMapUrlProvider
import com.vokilam.findmycar.domain.model.Location
import javax.inject.Inject

internal class MapboxStaticMapUrlProvider @Inject constructor() : StaticMapUrlProvider {

    override fun getLocationPreviewUrl(location: Location, zoom: Int, width: Int, height: Int): String {
        val loc = "${location.longitude},${location.latitude}"
        return Uri.Builder()
            .scheme("https")
            .authority("api.mapbox.com")
            .appendEncodedPath("styles/v1")
            .appendEncodedPath("mapbox/streets-v11") // map style
            .appendEncodedPath("static")
            .appendEncodedPath("pin-l-car+ff0000($loc)") // pin overlay
            .appendEncodedPath("$loc,$zoom") // center + zoom
            .appendEncodedPath("${width}x${height}@2x") // size
            .appendQueryParameter("access_token", BuildConfig.MAPBOX_API_KEY)
            .build()
            .toString()
    }
}