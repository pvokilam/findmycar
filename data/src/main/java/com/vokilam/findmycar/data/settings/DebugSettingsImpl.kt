package com.vokilam.findmycar.data.settings

import android.content.SharedPreferences
import com.vokilam.findmycar.domain.Constants
import com.vokilam.findmycar.domain.settings.DebugSettings
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_ACCURACY
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_STALENESS
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_LOCATION_UPDATES_TIMEOUT
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_HEIGHT
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_WIDTH
import com.vokilam.findmycar.domain.settings.DebugSettings.Companion.KEY_PREVIEW_ZOOM
import com.vokilam.findmycar.domain.settings.Settings
import javax.inject.Inject
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class DebugSettingsImpl @Inject constructor(
    prefs: SharedPreferences
) : DebugSettings, Settings by SettingsImpl(prefs)  {
    override var locationAccuracy by prefs.intPref(KEY_LOCATION_ACCURACY, 25)

    private var _locationStaleness by prefs.intPref(KEY_LOCATION_STALENESS, 30)
    override var locationStaleness: Duration
        get() = _locationStaleness.toDuration(DurationUnit.SECONDS)
        set(value) { _locationStaleness = value.toInt(DurationUnit.SECONDS) }

    private var _locationUpdatesTimeout by prefs.intPref(KEY_LOCATION_UPDATES_TIMEOUT, 10)
    override var locationUpdatesTimeout: Duration
        get() = _locationUpdatesTimeout.toDuration(DurationUnit.SECONDS)
        set(value) { _locationStaleness = value.toInt(DurationUnit.SECONDS) }

    override var previewZoom by prefs.intPref(KEY_PREVIEW_ZOOM, Constants.LOCATION_PREVIEW_ZOOM)
    override var previewWidth by prefs.intPref(KEY_PREVIEW_WIDTH, Constants.LOCATION_PREVIEW_WIDTH)
    override var previewHeight by prefs.intPref(KEY_PREVIEW_HEIGHT, Constants.LOCATION_PREVIEW_HEIGHT)
}