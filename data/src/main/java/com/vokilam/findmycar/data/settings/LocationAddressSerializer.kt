package com.vokilam.findmycar.data.settings

import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.model.LocationAddress
import org.json.JSONException
import org.json.JSONObject
import kotlin.time.Duration

internal object LocationAddressSerializer {

    private const val KEY_LAT = "lat"
    private const val KEY_LON = "lon"
    private const val KEY_ACC = "acc"
    private const val KEY_ADDR = "addr"

    fun decodeFromString(from: String): LocationAddress? {
        return try {
            JSONObject(from).run {
                LocationAddress(
                    Location(
                        getDouble(KEY_LAT),
                        getDouble(KEY_LON),
                        getDouble(KEY_ACC).toFloat(),
                        Duration.INFINITE
                    ),
                    getString(KEY_ADDR)
                )
            }
        } catch (e: JSONException) {
            null
        }
    }

    fun encodeToString(obj: LocationAddress?): String {
        if (obj == null) return ""

        return JSONObject().apply {
            put(KEY_LAT, obj.location.latitude)
            put(KEY_LON, obj.location.longitude)
            put(KEY_ACC, obj.location.accuracy)
            put(KEY_ADDR, obj.address)
        }.toString()
    }
}