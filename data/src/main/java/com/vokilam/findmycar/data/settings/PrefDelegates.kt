package com.vokilam.findmycar.data.settings

import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun SharedPreferences.stringPref(
    key: String,
    defaultValue: String = "",
    putOnce: Boolean = false
): ReadWriteProperty<Any, String> = object : ReadWriteProperty<Any, String> {
    override fun getValue(thisRef: Any, property: KProperty<*>): String = getString(key, defaultValue) ?: defaultValue
    override fun setValue(thisRef: Any, property: KProperty<*>, value: String) {
        if (putOnce && contains(key)) return
        edit { putString(key, value) }
    }
}

fun SharedPreferences.intPref(
    key: String,
    defaultValue: Int = 0,
    putOnce: Boolean = false
): ReadWriteProperty<Any, Int> = object : ReadWriteProperty<Any, Int> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Int = getInt(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) {
        if (putOnce && contains(key)) return
        edit { putInt(key, value) }
    }
}

fun SharedPreferences.booleanPref(
    key: String,
    defaultValue: Boolean = false,
    putOnce: Boolean = false
): ReadWriteProperty<Any, Boolean> = object : ReadWriteProperty<Any, Boolean> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean = getBoolean(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) {
        if (putOnce && contains(key)) return
        edit { putBoolean(key, value) }
    }
}

fun SharedPreferences.longPref(
    key: String,
    defaultValue: Long = 0L,
    putOnce: Boolean = false
): ReadWriteProperty<Any, Long> = object : ReadWriteProperty<Any, Long> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Long = getLong(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Long) {
        if (putOnce && contains(key)) return
        edit { putLong(key, value) }
    }
}

fun SharedPreferences.floatPref(
    key: String,
    defaultValue: Float = 0f,
    putOnce: Boolean = false
): ReadWriteProperty<Any, Float> = object : ReadWriteProperty<Any, Float> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Float = getFloat(key, defaultValue)
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Float) {
        if (putOnce && contains(key)) return
        edit { putFloat(key, value) }
    }
}

fun SharedPreferences.stringSetPref(
    key: String,
    defaultValue: Set<String> = emptySet(),
    putOnce: Boolean = false
): ReadWriteProperty<Any, Set<String>> = object : ReadWriteProperty<Any, Set<String>> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Set<String> = getStringSet(key, defaultValue) ?: defaultValue
    override fun setValue(thisRef: Any, property: KProperty<*>, value: Set<String>) {
        if (putOnce && contains(key)) return
        edit { putStringSet(key, value) }
    }
}
