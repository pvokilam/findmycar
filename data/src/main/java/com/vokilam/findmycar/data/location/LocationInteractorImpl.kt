package com.vokilam.findmycar.data.location

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Looper
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.CancellationTokenSource
import com.vokilam.findmycar.domain.model.Location
import com.vokilam.findmycar.domain.interactor.LocationInteractor
import com.vokilam.findmycar.domain.exception.InsufficientLocationSettingsException
import com.vokilam.findmycar.domain.exception.PermissionDeniedException
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class LocationInteractorImpl @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val fusedLocationProviderClient: FusedLocationProviderClient,
    private val locationSettingsClient: SettingsClient,
    private val mainLooper: Looper
) : LocationInteractor {

    override suspend fun getLastLocation(): Location? {
        L.debug { "getLastLocation()" }
        checkLocationPermission()

        return suspendCoroutine<android.location.Location?> { cont ->
            fusedLocationProviderClient.lastLocation
                .addOnSuccessListener { cont.resume(it) }
                .addOnFailureListener { cont.resumeWithException(it) }
        }?.toLocation()
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED) {
            throw PermissionDeniedException()
        }
    }

    override suspend fun getLocationUpdates(): Flow<Location> {
        L.debug { "getLocationUpdates()" }
        checkLocationPermission()
        checkLocationSettings()

        return callbackFlow {
            val locationRequest = LocationRequest.create().apply {
                interval = 1000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(res: LocationResult) {
                    L.verb { "onLocationResult(): $res" }
                    trySend(res.lastLocation.toLocation())
                }
            }

            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, mainLooper)

            awaitClose {
                fusedLocationProviderClient.removeLocationUpdates(locationCallback)
            }
        }
    }

    private suspend fun checkLocationSettings(): Boolean {
        val locationRequest = LocationRequest.create().apply {
            interval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val locationSettingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()

        return suspendCoroutine { cont ->
            locationSettingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener { cont.resume(true) }
                .addOnFailureListener {
                    when (it) {
                        is ResolvableApiException -> cont.resumeWithException(InsufficientLocationSettingsException(it))
                        else -> cont.resumeWithException(it)
                    }
                }
        }
    }

    override suspend fun getCurrentLocation(): Location {
        L.debug { "getCurrentLocation()" }
        checkLocationSettings()
        checkLocationPermission()

        return suspendCancellableCoroutine { cont ->
            val cts = CancellationTokenSource()
            fusedLocationProviderClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, cts.token)
                .addOnSuccessListener { cont.resume(it.toLocation()) }
                .addOnFailureListener { cont.resumeWithException(it) }
            cont.invokeOnCancellation { cts.cancel() }
        }
    }

    companion object {
        private val L = com.vokilam.findmycar.logger.Logger()
    }
}

private fun android.location.Location.toLocation(): Location {
    return Location(
        latitude,
        longitude,
        accuracy,
        elapsedRealtimeNanos.toDuration(DurationUnit.NANOSECONDS)
    )
}