package com.vokilam.findmycar.data.settings

import android.content.SharedPreferences
import com.vokilam.findmycar.domain.model.LocationAddress
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.AppSettings.Companion.KEY_LAST_LOCATION
import com.vokilam.findmycar.domain.settings.AppSettings.Companion.KEY_SELECTED_PAIRED_DEVICE_ADDRESS
import com.vokilam.findmycar.domain.settings.Settings
import javax.inject.Inject

internal class AppSettingsImpl @Inject constructor(
    prefs: SharedPreferences
) : AppSettings, Settings by SettingsImpl(prefs) {
    override var selectedPairedDeviceAddress by prefs.stringPref(KEY_SELECTED_PAIRED_DEVICE_ADDRESS)

    private var _lastLocation by prefs.stringPref(KEY_LAST_LOCATION)
    override var lastLocation: LocationAddress?
        get() = LocationAddressSerializer.decodeFromString(_lastLocation)
        set(value) {
            _lastLocation = LocationAddressSerializer.encodeToString(value)
        }
}
