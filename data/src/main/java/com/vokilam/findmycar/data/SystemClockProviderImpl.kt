package com.vokilam.findmycar.data

import android.os.SystemClock
import com.vokilam.findmycar.domain.provider.SystemClockProvider
import javax.inject.Inject
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

internal class SystemClockProviderImpl @Inject constructor() : SystemClockProvider {
    override fun elapsedRealtime(): Duration {
        return SystemClock.elapsedRealtime().toDuration(DurationUnit.MILLISECONDS)
    }
}