package com.vokilam.findmycar.data.di

import android.content.Context
import android.location.Geocoder
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.SettingsClient
import com.vokilam.findmycar.data.location.GeocoderInteractorImpl
import com.vokilam.findmycar.data.location.LocationInteractorImpl
import com.vokilam.findmycar.data.MapboxStaticMapUrlProvider
import com.vokilam.findmycar.data.SystemClockProviderImpl
import com.vokilam.findmycar.domain.interactor.GeocoderInteractor
import com.vokilam.findmycar.domain.interactor.LocationInteractor
import com.vokilam.findmycar.domain.provider.StaticMapUrlProvider
import com.vokilam.findmycar.domain.provider.SystemClockProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.*

@Module
@InstallIn(SingletonComponent::class)
internal abstract class DataModule {

    @Binds
    abstract fun bindLocationInteractor(impl: LocationInteractorImpl): LocationInteractor

    @Binds
    abstract fun bindGeocoderInteractor(impl: GeocoderInteractorImpl): GeocoderInteractor

    @Binds
    abstract fun bindLocationPreviewUrlProvider(impl: MapboxStaticMapUrlProvider): StaticMapUrlProvider

    @Binds
    abstract fun bindSystemClockProvider(impl: SystemClockProviderImpl): SystemClockProvider

    companion object {

        @Provides
        fun provideFusedLocationProviderClient(@ApplicationContext context: Context): FusedLocationProviderClient {
            // TODO: master 2/18/21 check google play services are installed https://developers.google.com/android/guides/setup#check-whether-installed
            return LocationServices.getFusedLocationProviderClient(context)
        }

        @Provides
        fun provideLocationSettingsClient(@ApplicationContext context: Context): SettingsClient {
            return LocationServices.getSettingsClient(context)
        }

        @Provides
        fun provideGeocoder(@ApplicationContext context: Context): Geocoder {
            return Geocoder(context, Locale.getDefault())
        }
    }
}