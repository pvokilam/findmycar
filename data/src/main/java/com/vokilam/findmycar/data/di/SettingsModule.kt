package com.vokilam.findmycar.data.di

import com.vokilam.findmycar.data.settings.AppSettingsImpl
import com.vokilam.findmycar.data.settings.DebugSettingsImpl
import com.vokilam.findmycar.domain.settings.AppSettings
import com.vokilam.findmycar.domain.settings.DebugSettings
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal abstract class SettingsModule {

    @Binds
    abstract fun bindAppSettings(impl: AppSettingsImpl): AppSettings

    @Binds
    abstract fun bindDebugSettings(impl: DebugSettingsImpl): DebugSettings
}