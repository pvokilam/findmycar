package com.vokilam.findmycar.data.location

import android.location.Geocoder
import com.vokilam.findmycar.domain.CoroutineDispatcherProvider
import com.vokilam.findmycar.domain.interactor.GeocoderInteractor
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

internal class GeocoderInteractorImpl @Inject constructor(
    private val geocoder: Geocoder,
    private val dispatcherProvider: CoroutineDispatcherProvider
) : GeocoderInteractor {

    override suspend fun getFromLocation(latitude: Double, longitude: Double): String {
        return withContext(dispatcherProvider.io) {
            try {
                @Suppress("BlockingMethodInNonBlockingContext")
                geocoder.getFromLocation(latitude, longitude, 1)
                    .firstOrNull()
                    ?.let {
                        if (it.maxAddressLineIndex == -1) NO_ADDRESS
                        else List(it.maxAddressLineIndex + 1, it::getAddressLine).joinToString(",")
                    } ?: NO_ADDRESS
            } catch (e: IOException) {
                NO_ADDRESS
            }
        }
    }

    companion object {
        private const val NO_ADDRESS = ""
    }
}